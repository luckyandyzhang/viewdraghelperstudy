## 基本用法

1. 通过ViewDragHelper的静态方法create创建实例
2. 实现`ViewDragHelper.Callback`,并实现当中的 ***几个主要的方法(下面有)***
3. 在`onInterceptTouchEvent()`方法里调用并返回ViewDragHelper的`shouldInterceptTouchEvent()`方法
4. 在`onTouchEvent()`方法里调用`processTouchEvent()`,并返回true

### ViewDragHelper.Callback 几个主要方法（必须）
- `tryCaptureView` 是否对当前触摸区域下的子View进行捕捉，返回true表示捕捉

- `clampViewPositionHorizontal` 限制当前子View的位置，水平方向

- `clampViewPositionVertical` 同上，垂直方向

### ViewDragHelper.Callback 常用的方法（非必须）
- `onViewReleased` 子View被释放时会回调此方法。在此方法中可以通过判断子View被释放前的位置（是否大于某个阀值），来决定接下来的动作（比如回弹，滚出屏幕等）

- `onViewPositionChanged` 子View位置变化是会回调此方法。此方法中可以根据子View位置变化，执行一些操作，比如改变background透明度等等

- `onEdgeDragStarted` 当父类View边缘刚开始拖动时回调此方法（此回调仅在mViewDragHelper.setEdgeTrackingEnabled()开启后才生效)

- `getViewVerticalDragRange` 返回子View在垂直方向上可以被拖动的最远距离，默认返回0。***如果子View消费了触摸事件（比如子View是Button的情况），又想要它在垂直方向上可以被拖动，就要返回大于0的数。***

- `getViewHorizontalDragRange` 同上，水平方向

### 其他一些需要注意的点
1. 在调用`settleCapturedViewAt()`、`flingCapturedView()`和`smoothSlideViewTo()`时，还需要实现父View的computeScroll()方法

2. `settleCapturedViewAt()`和`flingCapturedView()`只能在onViewReleased()中使用。在其他地方使用会报错！

### 扩展链接
> http://www.cnblogs.com/lqstayreal/p/4500219.html



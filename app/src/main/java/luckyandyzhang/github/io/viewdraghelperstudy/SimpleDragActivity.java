package luckyandyzhang.github.io.viewdraghelperstudy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

/**
 * Description:
 * Created by Andy on 15/12/10
 */
public class SimpleDragActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_drag);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SimpleDragActivity.this, "你追我,如果你追到我,我就让你嘿嘿嘿", Toast.LENGTH_LONG).show();
            }
        });
    }
}

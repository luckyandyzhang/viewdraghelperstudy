package luckyandyzhang.github.io.viewdraghelperstudy;

import android.content.Context;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Description:
 * Created by Andy on 15/12/10
 */
public class SimpleDragLayout extends FrameLayout {

    private ViewDragHelper viewDragHelper;

    public SimpleDragLayout(Context context) {
        this(context, null);
    }

    public SimpleDragLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SimpleDragLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return viewDragHelper.shouldInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        viewDragHelper.processTouchEvent(event);
        return true;
    }

    private void init() {
        viewDragHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                //允许捕捉任意子View
                return true;
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                return left;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                return top;
            }

            //由于子view中有Button,所以这里返回了非0数字,这样的话,既可以处理Button的点击事件,又可以拖动
            @Override
            public int getViewVerticalDragRange(View child) {
                return 1;
            }

            @Override
            public int getViewHorizontalDragRange(View child) {
                return 1;
            }


        });
    }
}

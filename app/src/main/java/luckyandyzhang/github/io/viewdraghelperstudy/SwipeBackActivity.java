package luckyandyzhang.github.io.viewdraghelperstudy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class SwipeBackActivity extends AppCompatActivity {

    private String TAG = "SwipeBackActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_back);

        SwipeBackLayout swipeBackLayout = (SwipeBackLayout) findViewById(R.id.swipeBack);
        swipeBackLayout.setOnSwipeBackListener(new SwipeBackLayout.OnSwipeBackListener() {
            @Override
            public void onSwipeBackStart() {
                Log.e(TAG, "onSwipeBackStart");
            }

            @Override
            public void onSwipeBackInProgress(int left, float f) {

            }

            @Override
            public void onSwipeBackFinished() {
                Log.e(TAG, "onSwipeBackFinished");
                finish();
                overridePendingTransition(0, 0);
            }
        });

    }

}

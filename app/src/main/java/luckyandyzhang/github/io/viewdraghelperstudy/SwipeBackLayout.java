package luckyandyzhang.github.io.viewdraghelperstudy;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by Andy on 15/10/30.
 */
public class SwipeBackLayout extends FrameLayout {

    private static final String TAG = "SwipeBackLayout";
    private ViewDragHelper mViewDragHelper;
    private View mCurtainView;
    private Rect mOriginalPosition;
    private boolean mSaveOriginalPosition;
    private boolean mSwipeBackTriggered;

    private OnSwipeBackListener mOnSwipeBackListener;

    public SwipeBackLayout(Context context) {
        this(context, null);
    }

    public SwipeBackLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeBackLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (!mSaveOriginalPosition) {
            mSaveOriginalPosition = true;
            mOriginalPosition = new Rect(left, top, right, bottom);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (getChildCount() != 1) {
            throw new IllegalStateException("This layout can contain one child !");
        }
        mCurtainView = getChildAt(0);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return mViewDragHelper.shouldInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mViewDragHelper.processTouchEvent(event);
        return true;
    }

    //绘制View的过程中会调用此方法,具体原理自行Google Scroller类
    @Override
    public void computeScroll() {
        //continueSettling(true)方法表示对被捕捉中的子View进行位置偏移.其中的参数'true'表示延迟DragState的回调(反正按照注释的说法就是,你在computeScroll()中,就一定是 continueSettling(true) 就对了)
        //如果这里返回true,表示整个滚动动作还没执行完,那么就需要继续调用此方法,那么我们就invalidate一下咯...
        if (mViewDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private void init() {
        //第一个参数 想要监听的ViewGroup
        //第二个参数 敏感度,此处值越大,说明对滑动事件的检测越敏感.
        //第三个参数 触摸事件信息的回调
        mViewDragHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {

            //此处可以过滤,想要捕捉的View
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return false;
            }

            //当用户释放这个(不再拖动)View时,触发此方法.
            //后面两个参数,代表释放该view时,手指离开屏幕那一刻的 水平,垂直方向的速度.
            //在这里可以根据View当前的位置,来决定接下来要执行的动作(比如,回弹,滚出屏幕等等...)
            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                super.onViewReleased(releasedChild, xvel, yvel);

                //如果子View拖动的位置超出了自身宽度的35%,那么让其滚出屏幕的可视范围
                if (Math.abs(releasedChild.getLeft()) >= releasedChild.getWidth() * 0.35) {
                    mViewDragHelper.settleCapturedViewAt(mOriginalPosition.width(), mOriginalPosition.top);
                    mSwipeBackTriggered = true;
                }
                //否则,回弹到初始位置
                else {
                    mViewDragHelper.settleCapturedViewAt(mOriginalPosition.left, mOriginalPosition.top);
                }

                //mViewDragHelper.settleCapturedViewAt()之后要手动invalidate一下 (实现机制也是调用Scroller的startScroll方法)
                ViewCompat.postInvalidateOnAnimation(SwipeBackLayout.this);

            }

            //子view位置改变时会回调此方法
            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                super.onViewPositionChanged(changedView, left, top, dx, dy);
                float delta = left * 1.0f / changedView.getWidth();
                if (mOnSwipeBackListener != null) {
                    mOnSwipeBackListener.onSwipeBackInProgress(left, delta);
                }
            }

            //View拖动状态回调
            @Override
            public void onViewDragStateChanged(int state) {
                super.onViewDragStateChanged(state);
                if (state == ViewDragHelper.STATE_IDLE && mSwipeBackTriggered && mOnSwipeBackListener != null) {
                    mOnSwipeBackListener.onSwipeBackFinished();
                }
            }

            //在边界拖动时回调
            @Override
            public void onEdgeDragStarted(int edgeFlags, int pointerId) {
                mViewDragHelper.captureChildView(mCurtainView, pointerId);
                if (mOnSwipeBackListener != null) {
                    mOnSwipeBackListener.onSwipeBackStart();
                }
            }

            //对子view水平的拖动位置进行限制
            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                return left > 0 ? Math.min(left, child.getWidth()) : 0;
            }


        });

        //设置左边缘触发
        mViewDragHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_LEFT | ViewDragHelper.EDGE_RIGHT);

    }

    public void setOnSwipeBackListener(OnSwipeBackListener onSwipeBackListener) {
        mOnSwipeBackListener = onSwipeBackListener;
    }

    public interface OnSwipeBackListener {
        void onSwipeBackStart();

        void onSwipeBackInProgress(int left, float f);

        void onSwipeBackFinished();
    }
}
